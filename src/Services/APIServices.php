<?php


namespace App\Services;


class APIServices
{

    private function generateGUID():string
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(mt_rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid =  substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16,
                4) . $hyphen . substr($charid, 20, 12);
        return $uuid;
    }

    public function generateAPIKey() :string
    {
        return $this->generateGUID();
    }

    public function validateAPIKeyForCompany(string $apiKey, int $account_id) :bool
    {

    }
}