<?php


namespace App\Services;


use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

class ToolServices implements LoggerAwareInterface
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @author Pradeep
     */
    public function setLogger(LoggerInterface $logger) :void
    {
        $this->logger = $logger;
    }
    /**
     * @param string $request
     * @return array
     * @author Pradeep
     */
    public function decodeRequest(string $request) :array
    {
        $content = [];
        if(empty($request)) {
            return $content;
        }
        try {
            $content_json = base64_decode($request);
            $content_arr = json_decode($content_json, true);

            if (!empty($content_arr)) {
                $content = $content_arr;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
        }
        return $content;
    }
}