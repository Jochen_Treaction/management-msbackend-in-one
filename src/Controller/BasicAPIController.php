<?php


namespace App\Controller;


use App\Services\APIServices;
use App\Services\ToolServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasicAPIController extends AbstractController
{

    /**
     * @Route("/generatecioapikey" , name="generateAPIKeyForCIO" , methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param APIServices $APIServices
     * @return JsonResponse
     * @author Pradeep
     */
    public function generateAPIKeyForCIO(
        Request $request,
        ToolServices $toolServices,
        APIServices $APIServices
    ): JsonResponse {
        $en_cnt = $request->getContent();
        $cnt = $toolServices->decodeRequest($en_cnt);
        if (empty($cnt) || !isset($cnt[ 'account_id' ])) {
            $status = [
                'api_key' => '',
                'status' => false,
                'message' => 'Invalid accountId',
            ];
            return $this->json($status);
        }
        $api_key = $APIServices->generateAPIKey();
        if (empty($api_key)) {
            $status = [
                'api_key' => '',
                'status' => false,
                'message' => 'Could not generate APIKey for CIO',
            ];
        } else {
            $status = [
                'api_key' => $api_key,
                'status' => true,
                'message' => 'success',
            ];
        }

        return $this->json($status);
    }

    /**
     * @Route("/check/alive", name="checkalive", methods={"GET"})
     */
    public function checkAlive(Request $request): JsonResponse
    {
        return $this->json([
            'your ip' => $request->getClientIp(),
            'alive' => true,
        ]);
    }

}
